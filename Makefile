#
# Makefile for the hypermodern Docker container
#

container_version = 1.0.0
container_registry = simune/foss
container_tag = $(container_registry):pyenv-base-$(container_version)

container_srcs = \
  Dockerfile \
  bin/load-hypermodern-env.sh \
  bin/show-current-env.sh \
  install-hypermodern-env.sh \
  packages-pyenv.txt \
  packages-system.txt

DEBUG ?= 0
ifeq ($(DEBUG),1)
  docker_opts = --progress=plain
else
  docker_opts = --progress=auto
endif

all all_targets default: build-image

upload-image: .upload-stamp

.upload-stamp: build-image
	docker push $(container_tag)
	touch .upload-stamp

build-image: .build-stamp

.build-stamp: $(container_srcs)
	DOCKER_BUILDKIT=1 \
	  docker build $(docker_opts) --tag $(container_tag) .
	touch .build-stamp

clean distclean:
	rm -f .build-stamp .upload-stamp

.PHONY: all all_targets clean default distclean build-image upload-image
