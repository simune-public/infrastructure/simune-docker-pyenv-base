Docker Container for Python Development with pyenv
==================================================

Building the image
------------------

Please install Docker on your computer first. See
https://docs.docker.com/get-docker/ for details.

To build a new image, just type:

    make

The default tag is set for publication on the Gitlab container registry of
Simune.

For more information about how to build and publish Docker images, please
consult the [official documentation of Docker](https://docs.docker.com/).


Using the image
---------------

The image provides a container with pre-installed Python interpreters managed
through [pyenv](https://github.com/pyenv/pyenv). It is possible to install
many more Python environments, please consult the documentation of pyenv for
details.

The rest of the environment is adapted for Simune from the [Hypermodern Python
Tutorial
Series](https://cjolowicz.github.io/posts/hypermodern-python-01-setup/) from
Claudio Jolowicz. All the details about developing Python applications within
this framework are explained there. Most of the infrastructure relies on
[poetry](https://python-poetry.org/) and
[nox](https://nox.thea.codes/en/stable/index.html).


Contributing
------------

Contributions and customizations are welcome. Before changing anything, please
create an issue summarizing your intentions.
