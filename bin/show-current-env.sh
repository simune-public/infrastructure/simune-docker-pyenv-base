#!/bin/bash

set +e

echo ""
echo "Hypermodern Development Environment:"
echo ""

pyenv --version
echo "pyenv virtualenvs:"
pyenv virtualenvs
echo "pyenv global:"
pyenv global
echo "pyenv local:"
pyenv local
echo ""

python --version
python -m pip --version
python -c 'import setuptools; print(f"setuptools {setuptools.version.__version__}")'
python -m wheel version
echo ""

echo "Nox $(nox --version 2>&1)"
poetry --version
echo ""

exit 0
