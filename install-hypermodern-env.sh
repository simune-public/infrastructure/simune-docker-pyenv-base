#!/bin/bash

# Stop at first error
set -e -o pipefail

# Check arguments
script_name=`basename "${0}"`
python_versions="${*}"
tmp_chk_args=`echo "${python_versions}" | sed -e 's/^[ ]*//; s/[ ]*$//'`
if test "${tmp_chk_args}" = ""; then
    echo "${script_name}: Error: Missing argument(s): Python version(s)" >&2
    exit 1
fi
unset tmp_chk_args

# Specify the base Python environment (the order matters)
python_packages="pip setuptools wheel nox poetry"
cat >constraints-simune.txt <<EOF
nox==2022.1.7
pip==22.0.4
poetry==1.1.13
setuptools==59.8.0
wheel==0.37.1
EOF

# Install pyenv
export PYENV_ROOT="/simune/pyenv"
curl -sSL "https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer" | bash

# Update the environment
export PATH="${PYENV_ROOT}/bin:${PATH}"
eval "$(pyenv init --path)"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

# Install the desired Python versions and their tooling in the correct order
for version in ${python_versions}; do
    env PYTHON_CONFIGURE_OPTS="--enable-shared" pyenv install "${version}"
    pyenv rehash
    pyenv shell "${version}"
    for pkg in ${python_packages}; do
        pip install --upgrade --constraint constraints-simune.txt "${pkg}"
    done
    poetry config "virtualenvs.in-project" "true"
    pyenv shell --unset
    pyenv rehash
done
pyenv global ${python_versions}

# Make the environment always available
for rc_file in .bashrc .profile; do
  echo 'export PATH="/simune/bin:${PATH}"' >>"${HOME}/${rc_file}"
  echo '. "/simune/bin/load-hypermodern-env.sh"' >>"${HOME}/${rc_file}"
done

# Report on the installed environments
pyenv global

# Clean-up the mess
rm -f constraints-simune.txt
